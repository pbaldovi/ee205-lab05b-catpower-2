///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file joule.h
/// @version 1.0
///
/// @author Paulo Baldovi <pbaldovi@hawaii.edu>
/// @date   15 Feb 2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

// A Joule represents the amount of electricity required to run a 1 W device for 1 s

const char JOULE         = 'j';
