///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.h
/// @version 1.0
///
/// @author Paulo Baldovi <pbaldovi@hawaii.edu>
/// @date   15 Feb 2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

const double GAS_GAL_IN_A_JOULE  = 1 / 1.213e8;
const char GAS_GAL               = 'g';

extern double fromGasToJoule(double gas);
extern double fromJouleToGas(double joule);
