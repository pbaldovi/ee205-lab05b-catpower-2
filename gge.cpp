///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.cpp
/// @version 1.0
///
/// @author Paulo Baldovi <pbaldovi@hawaii.edu>
/// @date   15 Feb 2022
///////////////////////////////////////////////////////////////////////////////

#include "gge.h"

double fromGasToJoule(double gas) {
   return gas / GAS_GAL_IN_A_JOULE;
}

double fromJouleToGas(double joule) {
   return joule * GAS_GAL_IN_A_JOULE;
}
